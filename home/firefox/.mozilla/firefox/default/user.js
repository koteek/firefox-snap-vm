// first run
user_pref("browser.aboutConfig.showWarning", false);  // don't show warning in about:config

// privacy
user_pref("privacy.resistFingerprinting", true);
user_pref("browser.contentblocking.category", "strict");
user_pref("app.update.url", "");  // don't update firefox (aus5.mozilla.org)
user_pref("media.gmp-manager.url", "");  // don't update plugins (aus5.mozilla.org)
user_pref("extensions.systemAddon.update.enabled", false);  // don't update system extensions (aus5.mozilla.org)
user_pref("extensions.update.enabled", false);  // don't update extensions (versioncheck-bg.addons.mozilla.org)
user_pref("browser.search.update", false);  // don't update search engines
user_pref("browser.search.suggest.enabled", false);  // disable search suggestions
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);
user_pref("app.shield.optoutstudies.enabled", false);
user_pref("network.captive-portal-service.enabled", false);  // disable requests to detectportal.firefox.com
user_pref("network.connectivity-service.enabled", false);  // disable requests to detectportal.firefox.com
user_pref("services.settings.server", "");  // disable requests to firefox.settings.services.mozilla.com
user_pref("browser.newtabpage.activity-stream.asrouter.providers.snippets", "");  // disable requests to snippets.cdn.mozilla.net
user_pref("browser.search.geoip.url", "");  // disable requests to location.services.mozilla.com
user_pref("browser.search.geoSpecificDefaults", false);  // disable requests to search.services.mozilla.com
user_pref("extensions.blocklist.enabled", false);  // disable requests to blocked.cdn.mozilla.net
user_pref("dom.push.enabled", false);  // disable push notifications (push.services.mozilla.com)
user_pref("app.normandy.enabled", false);  // disable normandy requests
user_pref("browser.safebrowsing.provider.mozilla.gethashURL", "");  // disable requests to shavar.services.mozilla.com
user_pref("browser.safebrowsing.provider.mozilla.updateURL", "");  // disable requests to shavar.services.mozilla.com
user_pref("browser.safebrowsing.provider.google.gethashURL", "");
user_pref("browser.safebrowsing.provider.google.updateURL", "");
user_pref("browser.safebrowsing.provider.google4.gethashURL", "");
user_pref("browser.safebrowsing.provider.google4.updateURL", "");
user_pref("security.OCSP.enabled", 0);  // disable certificate validation service requests
user_pref("media.gmp-gmpopenh264.enabled", false);  // disable cisco openh264 plugin download
user_pref("geo.enabled", false);  // disable location reporting
user_pref("dom.webaudio.enabled", false);  // disable audio
user_pref("media.navigator.enabled", false);  // disable list of media devices
user_pref("media.peerconnection.enabled", false);  // disable webrtc
user_pref("network.dns.disablePrefetch", true);  // don't "click" every link on every page

// control
user_pref("dom.event.contextmenu.enabled", false);  // don't allow websites to prevent use of right-click
user_pref("dom.event.clipboardevents.enabled", false);  // don't allow websites to prevent copy and paste
user_pref("browser.fixup.alternate.enabled", false);  // don't try to fix urls

// gui
user_pref("toolkit.cosmeticAnimations.enabled", false);
user_pref("general.smoothScroll", false);
user_pref("browser.tabs.loadInBackground", false);  // switch to new tab immediately
user_pref("browser.download.useDownloadDir", false);  // ask path before downloading
user_pref("signon.rememberSignons", false);  // don't ask to save passwords
user_pref("extensions.htmlaboutaddons.recommendations.enabled", false);  // don't show recommended extensions
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons", false);  // don't recommend extensions
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features", false);  // don't recommend features
user_pref("browser.newtabpage.enabled", false);  // show blank page on new tab
user_pref("browser.startup.homepage", "about:blank");  // show blank page on startup
user_pref("layout.spellcheckDefault", 0);  // disable spell checking
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"urlbar-container\",\"downloads-button\",\"bookmarks-menu-button\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"PersonalToolbar\":[\"personal-bookmarks\"]},\"seen\":[\"developer-button\"],\"dirtyAreaCache\":[\"nav-bar\"],\"currentVersion\":16,\"newElementCount\":3}");
user_pref("browser.pageActions.persistedActions", "{\"version\":1,\"ids\":[\"bookmark\",\"pinTab\",\"bookmarkSeparator\",\"copyURL\",\"emailLink\",\"addSearchEngine\",\"sendToDevice\",\"pocket\",\"screenshots_mozilla_org\"],\"idsInUrlbar\":[]}");
user_pref("browser.uidensity", 1);  // compact ui elements
user_pref("browser.urlbar.trimURLs", false);  // show full urls
user_pref("browser.urlbar.decodeURLsOnCopy", true);  // decode utf8 urls from urlbar

